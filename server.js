﻿let Express = require('express');
let multer = require('multer');
let bodyParser = require('body-parser');
let cors = require('cors');
let app = Express();
let port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
    ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';

const fs = require('fs');

app.use(cors());
app.use(bodyParser.json());
let Storage = multer.diskStorage({
    destination: function (res, file, callback) {
        callback(null, "./Images");
    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});

let upload = multer({ storage: Storage }).single("profile"); //Field name and max count

app.use('/images', Express.static(__dirname + '/Images'));

app.post("/api/upload", function (req, res) {
    upload(req, res, function (err) {
        if (err) {
            return res.end("Something went wrong!");
        }
        res.send({path: "/images/" + res.req.file.filename});
    });
});

app.get("/api/images", (req, res) => {
    let files = [];
    fs.readdirSync("./Images").forEach(file => {
        if (file.match(/.(jpg|jpeg|png|gif)$/i))
            files.push(file)
    });

    res.send(JSON.stringify(files));
})

app.listen(port, ip, function (a) {
    console.log("Listening to port", ip, ":", port);
});
